extern crate blurhash;
extern crate image;

#[cfg(test)]
mod verify_encode {
    use blurhash::blur_hash_for_pixels;
    use image::GenericImageView;

    fn compute_hash(file: &str, x_comp: u32, y_comp: u32) -> Result<String, String> {
        let image = match image::open(file) {
            Ok(image) => image,
            Err(err) => return Err(format!("{}", err)),
        };

        let width = image.width();
        let height = image.height();
        let img_rgb = image.to_rgb();
        let len = img_rgb.len();

        blur_hash_for_pixels(
            x_comp,
            y_comp,
            width,
            height,
            &img_rgb,
            len / height as usize,
        )
    }

    fn compare_hash(computed: &str, expected: &str) -> Result<(), String> {
        if computed == expected {
            Ok(())
        } else {
            Err(format!(
                "Computed hash does not match expected hash!\ncomputed: {}\nexpected: {}",
                computed, expected
            ))
        }
    }

    #[test]
    fn encode_4x3_test_01() -> Result<(), String> {
        let computed = compute_hash("./images/test_01.png", 4, 3)?;
        let expected = "LFE.@D9F01_2%L%MIVD*9Goe-;WB";

        compare_hash(&computed, expected)
    }
}
