extern crate blurhash;
extern crate image;

use blurhash::blur_hash_for_pixels;
use blurhash::pixels_for_blur_hash;
use image::GenericImageView;

fn compute_hash(file: &str, x_comp: u32, y_comp: u32) -> Result<(String, u32, u32), String> {
    let image = match image::open(file) {
        Ok(image) => image,
        Err(err) => return Err(format!("{}", err)),
    };

    let width = image.width();
    let height = image.height();
    let img_rgb = image.to_rgb();
    let len = img_rgb.len();

    let hash = blur_hash_for_pixels(
        x_comp,
        y_comp,
        width,
        height,
        &img_rgb,
        len / height as usize,
    )?;

    Ok((hash, width, height))
}

fn generate_image(hash: &str, width: u32, height: u32, punch: f32, filename: &str) -> Result<(), String> {
    let buffer = pixels_for_blur_hash(hash, width, height, punch)?;
    let mut img_buf = image::ImageBuffer::new(width, height);
    for (x, y, pixel) in img_buf.enumerate_pixels_mut() {
        let r = buffer[(y * width * 3 + x * 3) as usize];
        let g = buffer[(y * width * 3 + x * 3 + 1) as usize];
        let b = buffer[(y * width * 3 + x * 3 + 2) as usize];

        *pixel = image::Rgb::<u8>([r, g, b]);
    }
    match img_buf.save(filename) {
        Ok(_) => Ok(()),
        Err(err) => Err(format!("{}", err)),
    }
}

fn main() -> Result<(), String> {
    let component_settings = vec![
        (3, 3),
        (4, 3),
        (4, 4),
        (5, 5),
        (6, 6),
        (7, 7),
    ];
    let punch_settings = vec![1.0, 0.9, 0.8, 0.5, 0.2, 0.1];
    let example_files = vec![
        "example_a.JPG",
        "example_b.jpg",
        "example_c.jpg",
        "example_d.jpg",
    ];
    let mut example_hashes = Vec::with_capacity(component_settings.len() * example_files.len());

    // Encode
    for file in example_files {
        for (c1, c2) in component_settings.iter() {
            println!("Blurhash for `./images/{}`, with setting {}x{}:", file, c1, c2);
            let (hash, width, height) = compute_hash(&format!("./images/{}", file), *c1, *c2)?;
            println!("{}", hash);
            example_hashes.push((hash, file, width, height, c1, c2));
        }
    }

    // Decode
    for (hash, f, w, h, c1, c2) in example_hashes {
        for p in punch_settings.iter() {
            println!("Generating image from `{}`\n\thash: {}\n\tcomponent setting: {}x{}\n\tpunch: {}", f, hash, c1, c2, p);
            let filename = format!("./images/generated_{}_{}x{}_{}.jpg", f, c1, c2, p);
            match generate_image(&hash, w, h, *p, &filename) {
                Ok(_) => println!("=== Saved at `{}`", filename),
                Err(err) => return Err(err),
            }
        }
    }

    Ok(())
}
