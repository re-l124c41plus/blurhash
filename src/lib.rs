use std::f32;

static CHARACTERS: [char; 83] = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
    'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b',
    'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
    'v', 'w', 'x', 'y', 'z', '#', '$', '%', '*', '+', ',', '-', '.', ':', ';', '=', '?', '@', '[',
    ']', '^', '_', '{', '|', '}', '~',
];

pub fn blur_hash_for_pixels(
    x_components: u32,
    y_components: u32,
    width: u32,
    height: u32,
    image: &[u8],
    bytes_per_row: usize,
) -> Result<String, String> {
    let mut buffer = String::with_capacity(2 + 4 + (9 * 9 - 1) * 2 + 1);

    if x_components < 1 || x_components > 9 {
        return Err(format!(
            "x_components should be < 1 and > 9. Was: {}",
            x_components
        ));
    }
    if y_components < 1 || y_components > 9 {
        return Err(format!(
            "y_components should be < 1 and > 9. Was: {}",
            y_components
        ));
    }

    let mut factors = vec![0.0; (y_components * x_components * 3) as usize];

    for y in 0..y_components {
        for x in 0..x_components {
            let factor = multiply_basis_function(x, y, width, height, image, bytes_per_row);
            factors[(y * x_components * 3 + x * 3) as usize] = factor[0];
            factors[(y * x_components * 3 + x * 3 + 1) as usize] = factor[1];
            factors[(y * x_components * 3 + x * 3 + 2) as usize] = factor[2];
        }
    }

    let dc = &factors[0..3];
    let ac = &factors[3..];
    let ac_count = x_components * y_components - 1;

    let size_flag = (x_components - 1) + (y_components - 1) * 9;
    encode(size_flag, 1, &mut buffer);

    let maximum_value;
    if ac_count > 0 {
        let mut actual_maximum_value = 0.0;
        for i in 0..(ac_count * 3) {
            actual_maximum_value = f32::max(ac[i as usize].abs(), actual_maximum_value);
        }

        let quantised_maximum_value = f32::max(
            0.0,
            f32::min(82.0, (actual_maximum_value * 166.0 - 0.5).floor()),
        ) as u32;
        maximum_value = (quantised_maximum_value + 1) as f32 / 166.0;
        encode(quantised_maximum_value, 1, &mut buffer);
    } else {
        maximum_value = 1.0;
        encode(0, 1, &mut buffer);
    }

    encode(encode_dc(dc[0], dc[1], dc[2]), 4, &mut buffer);

    for i in 0..ac_count {
        encode(
            encode_ac(
                ac[(i * 3) as usize],
                ac[(i * 3 + 1) as usize],
                ac[(i * 3 + 2) as usize],
                maximum_value,
            ),
            2,
            &mut buffer,
        );
    }

    Ok(buffer)
}

pub fn pixels_for_blur_hash(hash: &str, width: u32, height: u32, punch: f32) -> Result<Vec<u8>, String> {
    if hash.len() == 0 {
        return Err(format!("Provided string is empty!"));
    }

    let size_flag = decode(&hash[0..1])?;
    let num_y = (size_flag / 9) + 1;
    let num_x = (size_flag % 9) + 1;

    let quantised_maximum_value = decode(&hash[1..2])?;
    let maximum_value = (quantised_maximum_value as f32 + 1.0) / 166.0;

    let expected_len = (4 + 2 * num_x * num_y) as usize;
    if hash.len() != expected_len {
        return Err(format!("Hash has wrong length! Len: {}, Expected: {}", hash.len(), expected_len));
    }

    let colors = {
        let cap = (num_x * num_y) as usize;
        let mut res = Vec::with_capacity(cap);
        for i in 0..cap {
            if i == 0 {
                let value = decode(&hash[2..6])?;
                res.push(decode_dc(value));
            } else {
                let start_index = 4 + i * 2;
                let value = decode(&hash[start_index..(start_index + 2)])?;
                res.push(decode_ac(value as f32, maximum_value as f32 * punch));
            }
        }

        res
    };

    let mut bitmap = vec![0; (width * height * 3) as usize];

    for y in 0..height {
        for x in 0..width {
            let mut r = 0.0;
            let mut g = 0.0;
            let mut b = 0.0;

            for j in 0..num_y {
                for k in 0..num_x {
                    let basis = ((f32::consts::PI * x as f32 * k as f32 / width as f32).cos() * (f32::consts::PI * y as f32 * j as f32 / height as f32).cos()) as f32;
                    let color = &colors[(k + j * num_x) as usize];
                    r += color[0] * basis;
                    g += color[1] * basis;
                    b += color[2] * basis;
                }
            }

            bitmap[(y * width * 3 + x * 3) as usize] = linear_to_srgb(r) as u8;
            bitmap[(y * width * 3 + x * 3 + 1) as usize] = linear_to_srgb(g) as u8;
            bitmap[(y * width * 3 + x * 3 + 2) as usize] = linear_to_srgb(b) as u8;
        }
    }

    Ok(bitmap)
}

fn multiply_basis_function(
    x_component: u32,
    y_component: u32,
    width: u32,
    height: u32,
    image: &[u8],
    bytes_per_row: usize,
) -> Vec<f32> {
    let mut r = 0.0;
    let mut g = 0.0;
    let mut b = 0.0;
    let normalisation = if x_component == 0 && y_component == 0 {
        1.0
    } else {
        2.0
    };

    for y in 0..height {
        for x in 0..width {
            let x_part = (f32::consts::PI * x_component as f32 * x as f32 / width as f32).cos();
            let y_part = (f32::consts::PI * y_component as f32 * y as f32 / height as f32).cos();
            let basis = x_part * y_part;

            r += basis * srgb_to_linear(image[3 * x as usize + y as usize * bytes_per_row] as u32);
            g += basis * srgb_to_linear(image[3 * x as usize + 1 + y as usize * bytes_per_row] as u32);
            b += basis * srgb_to_linear(image[3 * x as usize + 2 + y as usize * bytes_per_row] as u32);
        }
    }

    let scale = normalisation / (width * height) as f32;

    vec![r * scale, g * scale, b * scale]
}

fn linear_to_srgb(value: f32) -> u32 {
    let v = f32::max(0.0, f32::min(1.0, value));
    if v <= 0.003_130_8 {
        (v * 12.92 * 255.0 + 0.5) as u32
    } else {
        ((1.055 * v.powf(1.0 / 2.4) - 0.055) * 255.0 + 0.5) as u32
    }
}

fn srgb_to_linear(value: u32) -> f32 {
    let v = value as f32 / 255.0;
    if v <= 0.04045 {
        v / 12.92
    } else {
        ((v + 0.055) / 1.055).powf(2.4)
    }
}

fn encode(value: u32, length: u32, destination: &mut String) {
    let mut divisor = 1;
    for _i in 0..(length - 1) {
        divisor *= 83;
    }

    for _i in 0..length {
        let digit = (value / divisor) % 83;
        divisor /= 83;
        destination.push(CHARACTERS[digit as usize]);
    }
}

fn encode_dc(r: f32, g: f32, b: f32) -> u32 {
    let rounded_r = linear_to_srgb(r);
    let rounded_g = linear_to_srgb(g);
    let rounded_b = linear_to_srgb(b);

    (rounded_r << 16) + (rounded_g << 8) + rounded_b
}

fn encode_ac(r: f32, g: f32, b: f32, maximum_value: f32) -> u32 {
    let quant_r = f32::max(
        0.0,
        f32::min(18.0, (sign_pow(r / maximum_value, 0.5) * 9.0 + 9.5).floor()),
    );
    let quant_g = f32::max(
        0.0,
        f32::min(18.0, (sign_pow(g / maximum_value, 0.5) * 9.0 + 9.5).floor()),
    );
    let quant_b = f32::max(
        0.0,
        f32::min(18.0, (sign_pow(b / maximum_value, 0.5) * 9.0 + 9.5).floor()),
    );

    (quant_r * 19.0 * 19.0 + quant_g * 19.0 + quant_b) as u32
}

fn sign_pow(value: f32, exp: f32) -> f32 {
    // TODO: use copysign
    let pow = value.abs().powf(exp);
    let pow_sign = pow.signum();
    let val_sign = value.signum();

    if (pow_sign - val_sign).abs() < f32::EPSILON {
        pow
    } else if pow_sign < 0.0 {
        pow * pow_sign
    } else {
        pow * val_sign
    }
}

fn decode(value: &str) -> Result<u32, String> {
    let mut res = 0;
    for c in value.chars() {
        match CHARACTERS.iter().position(|&t| t == c) {
            Some(pos) => res = res * 83 + pos,
            None => return Err(format!("Hash contains invalid character: {}", c)),
        }
    }

    Ok(res as u32)
}

fn decode_dc(value: u32) -> Vec<f32> {
    let int_r = value >> 16;
    let int_g = (value >> 8) & 255;
    let int_b = value & 255;

    vec![srgb_to_linear(int_r), srgb_to_linear(int_g), srgb_to_linear(int_b)]
}

fn decode_ac(value: f32, maximum_value: f32) -> Vec<f32> {
    let quant_r = (value / (19.0 * 19.0)).floor();
    let quant_g = (value / 19.0).floor() % 19.0;
    let quant_b = value % 19.0;

    vec![
        sign_pow((quant_r - 9.0) / 9.0, 2.0) * maximum_value,
        sign_pow((quant_g - 9.0) / 9.0, 2.0) * maximum_value,
        sign_pow((quant_b - 9.0) / 9.0, 2.0) * maximum_value,
    ]
}
